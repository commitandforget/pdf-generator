<?php
// 'type' => 'paint',
// 'ids' => array(664,587),
// 'custon' => array(
//   'element_name' => 'element_qty',
//   'element_name' => 'element_qty',
//   'element_name' => 'element_qty',
// )
function render_print_page($data){
  $content = apply_filters( 'pdf_print_order', '', $data);
  //do it!
  ob_start();
  //print styles
  ?>
  <!doctype html>

  <html lang="pl-PL">
    <head>
      <meta charset="UTF-8" />
      <meta name="robots" content="noindex,follow" />
      <title>Print</title>
    </head>
    <body>
      <?php echo $content; ?>
    </body>
    <style>
    body {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0;
      background-color: #fff;
      font: 12pt "Open Sans", "Arial", sans-serif;
    }
    * {
      box-sizing: border-box;
      -moz-box-sizing: border-box;
    }
    .page {
      width: 210mm;
      min-height: 297mm;
      padding: 5mm 10mm;
      margin: 10mm auto;
      border: 1px #D3D3D3 solid;
      border-radius: 5px;
      background: white;
      box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
      padding: 1cm;
      border: 5px red solid;
      height: 257mm;
      outline: 2cm #FFEAEA solid;
    }
    .t-left {
      float: left;
    }

    .t-right {
      float: right;
    }
    .t-center{
      text-align: center;
    }
    .column-half{
      width:48%;
      float: left;
      margin-right: 2%;
    }
    .column-half.last{
      margin-right: 0%;
    }
    .clear {
      clear: both;
    }
    @page {
      size: A4;
      margin: 0;
    }
    @media print {
        html, body {
          width: 210mm;
          height: 297mm;
        }
        .page {
          margin: 0;
          border: initial;
          border-radius: initial;
          width: initial;
          min-height: initial;
          box-shadow: initial;
          background: initial;
          page-break-after: always;
        }
    }
    .variation dt {
      float: left;
      width: 50%;
      font-weight: bold;
      text-align: right;
      padding-right: 15px;
    }

    .variation dd {
      margin-left: 50%;
    }

    .variation p {
      padding: 0;
      margin: 0;
    }

    .variation:after {
      content: "";
      display:block;
      clear:both;
    }
    .variation dd:after {
      content: "";
      display:block;
      clear:both;
    }
    h3 {
      font-weight: normal;
    }

    h3 span {
      font-weight: bold;
    }
    .images-wrap{
      text-align: center;
    }
    .images-layared {
      position: relative;
      display: inline-block;
    }

    .images-layared img.wp-post-image {
      position: static;
    }

    .images-layared img {
      position: absolute;
      left: 0;
      top: 0;
    }

    table.table {
      width: 100%;
      border-spacing: 0;
      border-collapse: collapse;
      text-align: left;
    }

    table.table th, table.table td {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: bottom;
      border-bottom: 1px solid #ddd;
    }
    table.table-inline-summary th, table.table-inline-summary td{
      vertical-align: middle;
          width: 50%;
    }

    table.table thead th {
      border-bottom: 2px solid #ddd;
    }

    table.table thead th, table.table tfoot td {
      background: #F5F5F5;
      font-weight: bold;
    }
    table.table tfoot td{
      border-top: 2px solid #ddd;
    }
    table.table tbody tr:nth-of-type(odd) {
      background-color: #f9f9f9;
    }
    .company-header {
      text-align: center;
      display: block;
    }
    .company-header img {
      max-width: 100%;
      max-height: 130px;
    }
    </style>
  </html>
<?php
return base64_encode(trim(preg_replace('/\s\s+/', ' ', ob_get_clean())));
} ?>
